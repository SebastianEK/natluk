import os

from django import forms
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string

# Declaring ContactForm with username,email,message and sending email to administrators


class ContactForm(forms.Form):
    username = forms.CharField(
        required=True,
        label='Imie/Nick',
        widget=forms.TextInput(
            attrs={'placeholder': 'np. SebastianEKGT#7765'})
    )

    email = forms.EmailField(
        required=True,
        label='Email',
        widget=forms.TextInput(
            attrs={'placeholder': 'np. severussnape@hogwart.com'})
    )
    message = forms.CharField(
        required=True,
        label='Wiadomość',
        widget=forms.Textarea(
            attrs={'placeholder': 'Treść wiadomości...'})
    )

    def send_email(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')
        message = self.cleaned_data.get('message')

        ctx = {'username': username, 'email': email,
               'message': message, 'subject': 'Contact Page'}
        html_message = render_to_string('email.html', ctx)

        send_mail(
            subject='Contact',
            message=message,
            from_email=None,
            recipient_list=[os.environ['EMAIL_HOST_USER']],
            html_message=html_message,
            fail_silently=False,
        )
