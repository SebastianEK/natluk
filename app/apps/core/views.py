import json

import urllib
import urllib.request

from django.views.generic import FormView

from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.conf import settings

from .forms import ContactForm

# Contact View


class ContactView(FormView):
    template_name = 'contact.html'
    form_class = ContactForm
    success_url = reverse_lazy('core:contact')

    def get_context_data(self, **kwargs):
        # Add Recaptcha Public Key to get request
        data = super(ContactView, self).get_context_data(**kwargs)
        data['RECAPTCHA_PUBLIC_KEY'] = settings.RECAPTCHA_PUBLIC_KEY
        return data

    def form_valid(self, form):
        # Verify recaptcha, send email and verif
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        values = {
            'secret': settings.RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode("utf-8")
        req = urllib.request.Request(
            'https://www.google.com/recaptcha/api/siteverify', data)
        response = urllib.request.urlopen(req)
        result = json.load(response)

        if result:
            form.send_email()
            messages.success(self.request, 'Form submission successful')
            return HttpResponseRedirect(self.request.path_info)
        else:
            messages.error(self.request, 'Form submission failed!')
            return HttpResponseRedirect(self.request.path_info)
