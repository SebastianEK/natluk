const messages = document.querySelector('.messages')

if (messages != null) {
    document.addEventListener('DOMContentLoaded', () => {
        messages.style.transform = 'translateX(0)'
        setTimeout(hide, 5000)
    })
    messages.addEventListener('click', hide)

    function hide() {
        messages.style.transform = 'translateX(150%)'
    }
}

function show_menu() {
    menu = document.querySelector('.mobile_menu')
    menu.classList.toggle('active')
}

