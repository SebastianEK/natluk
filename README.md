# Natluk Project

## Description
TODO: Create project description.

## Contributing
1. Join discord [Natluk Python](https://discord.gg/YQQreR)
2. Ask **Xarabek** for access to gitlab project and Trello.
3. Setup your workplace:
	1. Generate SSH key: [link to guide!](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
	2. Add your SSH key to gitlab.
		1. Go to settings -> SSH Keys.
		2. Copy and paste content of the generated id_rsa.pub file.
		3. Add title (no need for adding expiration date)
	3. Clone the repository git clone ```$ git clone git@gitlab.com:natluk/natluk.git```
	4. Assign yourself to the issue on Trello and start development of your feature.


### Before you start development
1. Whole development must be done in english.
2. We focus on developing clean code - as good as we possibly can and the docummentation is part of this. Every class and every function has to have short description of it's resposibility.
3. When you add new package to the project always specify version in the requirements.txt

### Working with branches and setting up Merge Request
1. Create new branch basing on the **develop** branch using the following format: 
```<feature/fix/hotfix>/<task id from gitlab/<short information about task (or title from gitlab)>```
using following commands:
```console
$ git fetch
$ git pull
$ git checkout develop
$ git checkout -b  <branch_name>
```

2. When you are developing the feature, don't add unnecessary files - you will be asked to remove them if you add them. Besides that do typical git workflow, some things you may want to use.
	 1. Add files to the commit: 
    	 ```console
        $ git add <file/directory>
        ```
	 2. Commit the changes and add message: 
    	 ```console
        $ git commit -m "<commit message>"
        ```
	 3. [Guide on how to write good commit message](https://learn-the-web.algonquindesign.ca/topics/commit-message-cheat-sheet/).

3. To push your code to the remote branch you have to setup upstream:

    ```console
    $ git push -u origin <branch_name>
    ```

    after upstream has been setup, to push your changes just use:
    ```console
    $ git push
    ```
4. Create merge request by clicking link generated in console after pushing your branch.
	1. Title has to contain describing title and ideally Trello card link wrapped in square brackets. While developing issue,prefix title with "Draft: "
	2. Make sure "Delete source branch when merge request is accepted." box is ticked.
	3. Choose target branch to the **develop** branch.
5. When you finish implementing your feature and you have tested it you need to ask for review of your code. To do this:
	1. mention developers ```@natluk``` in the merge request
	2. Write on the #natluk-project_code_review channel on discord. Message should only contain link to the Merge Request on gitlab + optionally some short description or message to encourage people to quickly review your code - everything in one message. No talking on the chanel!
6. Discuss every comment if you don't agree with the commenter and fix every commented issue in your code - even if it's only a typo in the doc-string. 
7. Our branching workflow:
    ```mermaid
    graph TD
      A[master branch] -->B
      B[User creates feature branch from the develop branch ] --> C
      C[User develops his feature and tests it on his branch] --> D
      D[Merge request is accepted and merged into develop branch] --> E
      E[After few features are developed maintainers of the repository may merge them to the QA branch] --> F
      F{Testing discovered new bugs?}
      F-->|yes|G
      F-->|no|H
      G[Tester raises trello ticket]-->B
      H[develop branch is merged to the master] --> I
      I[Repository maintainer merges changes to the production branch]
    ```
8. After double checking if everything is correct you are allowed to merge your changes.
**If you need any help or you have any questions/remarks write on discord channel $natluk-projekt and mention user binq**

Requirements
------------
Docker container template using docker-compose with Django, Nginx, uWSGI and Postgres
In order use this template `bash`, `Docker` and `Docker-Compose` are required (docker>19.03 CE and docker-compose>1.26). \

Installation
------------

##### 1. Code / Repository

Repository can be cloned by one of following commands:
* Via SSH: `git clone git@gitlab.com:natluk/natluk.git`
* Via HTTPS: `git clone https://gitlab.com/natluk/natluk.git`

##### 2. Docker containers

Upon cloning the repository `Natluk` project can be built and ran using commands:

1. `./run.sh -stp` to initialize local setup and config development environment.
2. `./run.sh -u` to start local containers with follow output or `./run.sh -u` without output.
3. Optional. If you'd like to create super user account run `./run.sh -csu <password>` f.g. `./run.sh -csu admin`. Default admin username is `admin`.

Local web address: `http://localhost:8000/` \
Django Admin Panel address: `http://localhost:8000/admin` \
WDB address: `http://localhost:1984`

Instructions
-----

##### Usage of `./run.sh` file:

```
Usage (params with '*' are optional):
./run.sh                                             -> UP containers in detach mode
./run.sh bash|-sh                                    -> Open bash in main container
./run.sh build|-b <params*>                          -> BUILD containers
./run.sh build-force|-bf <params*>                   -> Force build containers (with params no-cache, pull)
./run.sh custom_command|-cc                          -> Custom docker-compose command
./run.sh create_django_secret|-crs                   -> Create Django Secret Key
./run.sh create_superuser|-csu <password>            -> Create default super user
./run.sh down|-dn                                    -> DOWN (stop and remove) containers
./run.sh downv|-dnv                                  -> DOWN (stop and remove with volumes) containers
./run.sh init|-i <project name> <django version*>    -> Initial setup and config development environment with creating new django project
./run.sh help|-h                                     -> Show this help message
./run.sh logs|-l <params*>                           -> LOGS from ALL containers
./run.sh logsf|-lf <params*>                         -> LOGS from ALL containers with follow option
./run.sh shell|-sl                                   -> Open shell in main container
./run.sh shell_plus|-sp                              -> Open shell plus (only if django_extensions installed) in main container
./run.sh makemigrate|-mm <params*>                   -> Make migrations and migrate inside main container
./run.sh notebook|-nb                                -> Run notebook (only if django_extensions installed)
./run.sh recreate|-rec <params*>                     -> Up and recreate containers
./run.sh recreated|-recd <params*>                   -> Up and recreate containers in detach mode
./run.sh restart|-r <params*>                        -> Restart containers
./run.sh rm|-rm <params*>                            -> Remove force container
./run.sh setup|-stp                                  -> Setup project for local development
./run.sh stop|-s <params*>                           -> Stop containers
./run.sh test|-t <params*>                           -> Run tests
./run.sh up|-u <params*>                             -> UP containers with output
```

You could also run `./run.sh help` or `./run.sh -h` in terminal to show this message.
